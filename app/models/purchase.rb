class Purchase < ActiveRecord::Base
  belongs_to :dinner
  belongs_to :user
  attr_accessible :dinner_id, :user_id
end
