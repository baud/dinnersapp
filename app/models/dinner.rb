class Dinner < ActiveRecord::Base
  attr_accessible :date, :description, :name, :restaurant_id, :price, :photo
  validates :name, :date, :price, :restaurant_id, :presence => true
  paginates_per 5

  belongs_to :restaurant
  has_many :purchases
  after_initialize :default_values

  scope :upcoming, lambda {
    where("date >= ?", Date.today)
  }

  scope :past, lambda {
    where("date < ? ", Date.today)
  }

  has_attached_file :photo,
    :styles => {
    :thumb  => "100x100",
    :medium => "200x200",
    },
    :storage => :s3,
    :bucket => 'dinnersapp',
    :path => ":attachment/dinners/:id/:style.:extension",
    :s3_credentials => {
      :access_key_id => ENV['S3_KEY'],
      :secret_access_key => ENV['S3_SECRET']
    },
    :default_url => '/unknown.jpg'

   private
     def default_values
       self.date ||= Time.now
     end
end
