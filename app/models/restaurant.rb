class Restaurant < ActiveRecord::Base
  attr_accessible :address, :description, :name, :photo, :latitude, :longitude
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
  paginates_per 20

  has_many :dinners

  has_attached_file :photo,
    :styles => {
    :thumb  => "100x100",
    :medium => "200x200",
    },
    :storage => :s3,
    :bucket => 'dinnersapp',
    :path => ":attachment/restaurants/:id/:style.:extension",
    :s3_credentials => {
      :access_key_id => ENV['S3_KEY'],
      :secret_access_key => ENV['S3_SECRET']
    },
    :default_url => '/unknown.jpg'
end
