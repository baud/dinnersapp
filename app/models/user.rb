class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  has_many :purchases

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :name, :image, :password, :password_confirmation, :remember_me, :provider, :uid
  # attr_accessible :title, :body
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    if user = User.where(:provider => auth.provider, :uid => auth.uid).first
      if auth.info.image.present?
        user.update_attribute(:image, auth.info.image.gsub('square', ''))
      end
    else
      user = User.create(name:auth.extra.raw_info.name,
                           provider:auth.provider,
                           uid:auth.uid,
                           email:auth.info.email,
                           password:Devise.friendly_token[0,20],
                           image:auth.info.image.gsub('square', '')
                           )
    end
    user
  end
end
