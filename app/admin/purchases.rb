ActiveAdmin.register Purchase do
  index do
    column :user
    column :dinner
    column :created_at
    default_actions
  end
end
