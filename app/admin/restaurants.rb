ActiveAdmin.register Restaurant do
  index do
    column :name
    column :address
    column :latitude
    column :longitude
    column :created_at
    default_actions
  end

  form :html => { :enctype => "multipart/form-data" } do |f|
        f.inputs "Base" do
          f.input :name, :label => "Name"
          f.input :description, :label => "Description"
          f.input :address, :label => "Address"
          f.input :photo, :as => :file, :label => "Foto", :hint => f.template.image_tag(f.object.photo.url(:thumb))
        end
        f.buttons
      end
end
