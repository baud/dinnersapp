ActiveAdmin.register Dinner do
  index do
    column :name
    column "Date", :date
    column :restaurant
    column :price do |dinner|
      number_to_currency dinner.price, :unit => "&euro;"
    end
    default_actions
  end
  form :html => { :enctype => "multipart/form-data" } do |f|
        f.inputs "Base" do
          f.input :name, :label => "Name"
          f.input :description, :label => "Description"
          f.input :date, :label => "Description"
          f.input :price, :label => "Price"
          f.input :restaurant, :label => "Restaurant"
          f.input :photo, :as => :file, :label => "Photo", :hint => f.template.image_tag(f.object.photo.url(:thumb))
        end
        f.buttons
      end

end
