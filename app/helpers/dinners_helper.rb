module DinnersHelper
  def can_i_book_it? dinner
    exists = Purchase.where(:dinner_id => dinner.id, :user_id => current_user.id)
    exists.empty?
  end

  def date_bookable? dinner
    date_forward = dinner.date > Time.now
    date_forward
  end
end
