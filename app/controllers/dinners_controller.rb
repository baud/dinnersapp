class DinnersController < ApplicationController
  # GET /dinners/1
  # GET /dinners/1.json
  def show
    @dinner = Dinner.find(params[:id])
    @purchase_users = User.joins(:purchases => :dinner).where(:dinners => {:id => params[:id]})

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dinner }
    end
  end
end
