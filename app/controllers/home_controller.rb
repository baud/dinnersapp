class HomeController < ApplicationController
  def index
    @dinners = Dinner.upcoming.order(:date).page params[:page]

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dinners }
    end
  end

  def past
    @dinners = Dinner.past.order(:date).page params[:page]

    respond_to do |format|
      format.html # past.html.erb
      format.json { render json: @dinners }
    end
  end

  def about
    respond_to do |format|
      format.html # about.html.erb
    end
  end
end
