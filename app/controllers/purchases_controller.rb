class PurchasesController < InheritedResources::Base
  before_filter :authenticate_user!, :except => [:new, :create]

  def new
    # Validate its now possible to create a new purchase if user has already bought
    exists = Purchase.where(:dinner_id => params[:dinner_id], :user_id => current_user.id)
    @dinner = Dinner.find(params[:dinner_id])

    respond_to do |format|
      if exists.empty?
        @purchase = Purchase.new
        @user = User.find(current_user.id)
        format.html # new.html.erb
        format.json { render json: @purchase }
      else
        format.html { redirect_to @dinner, notice: 'You have already bought a ticket for this dinner.' }
      end
    end
  end

  def create
    @dinner = Dinner.find(params[:dinner_id])
    params.merge!({:user_id => current_user.id}) # Done server side to avoid user fiddling with user_id parameter
    @purchase = @dinner.purchases.new(:user_id => params[:user_id])
    puts "--------PARAMS #{params}"
    puts " CURRENT USER #{current_user.id}"

    respond_to do |format|
      if @purchase.save
        format.html { redirect_to @dinner, notice: 'You have successfully bought a ticket for this dinner.' }
        format.json { render json: @purchase, status: :created, location: @purchase }
      else
        format.html { render action: "new" }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end
end
