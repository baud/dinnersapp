class RestaurantsController < InheritedResources::Base
  # GET /restaurants
  # GET /restaurants.json
  def index

    if params[:distance].present?
      distance = params[:distance].first.to_i
    else
      distance = 10
      params[:distance] = distance
    end

    if params[:search].present?
      @restaurants = Restaurant.near(params[:search], distance, :order => :distance).page params[:page]
    else
      ip = request_ip
      @response = Geocoder.search(ip).first
      @restaurants = Restaurant.near("#{@response.city},#{@response.country}", distance, :order => :distance).page params[:page]
    end

    #@restaurants = Restaurant.page params[:page]
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @restaurants }
    end
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
    @restaurant = Restaurant.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @restaurant }
    end
  end
end
