class AddPriceToDinner < ActiveRecord::Migration
  def change
    add_column :dinners, :price, :decimal
  end
end
