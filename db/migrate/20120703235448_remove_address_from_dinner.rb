class RemoveAddressFromDinner < ActiveRecord::Migration
  def change
    remove_column :dinners, :address
  end
end
