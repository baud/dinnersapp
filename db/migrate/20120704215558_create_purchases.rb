class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :dinner
      t.references :user

      t.timestamps
    end
    add_index :purchases, :dinner_id
    add_index :purchases, :user_id
  end
end
