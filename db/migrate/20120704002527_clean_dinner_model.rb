class CleanDinnerModel < ActiveRecord::Migration
  def change
    rename_column :dinners, :datetime, :date
    change_column :dinners, :date, :datetime
  end
end
