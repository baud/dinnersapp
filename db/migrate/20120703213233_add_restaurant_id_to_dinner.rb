class AddRestaurantIdToDinner < ActiveRecord::Migration
  def change
    add_column :dinners, :restaurant_id, :integer
    add_index :dinners, :restaurant_id
  end
end
