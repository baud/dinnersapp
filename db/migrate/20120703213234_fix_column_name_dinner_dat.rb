class FixColumnNameDinnerDat < ActiveRecord::Migration
  def change
    rename_column :dinners, :date, :time
  end
end
