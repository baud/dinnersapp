class CreateDinners < ActiveRecord::Migration
  def change
    create_table :dinners do |t|
      t.string :name
      t.text :description
      t.time :date
      t.string :address

      t.timestamps
    end
  end
end
