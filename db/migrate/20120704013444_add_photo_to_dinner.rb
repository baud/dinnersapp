class AddPhotoToDinner < ActiveRecord::Migration
  def change
    add_column :dinners, :photo_file_name,    :string
    add_column :dinners, :photo_content_type, :string
    add_column :dinners, :photo_file_size,    :integer
    add_column :dinners, :photo_updated_at,   :datetime
  end
end
