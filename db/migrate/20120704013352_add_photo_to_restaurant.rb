class AddPhotoToRestaurant < ActiveRecord::Migration
  def change
    add_column :restaurants, :photo_file_name,    :string
    add_column :restaurants, :photo_content_type, :string
    add_column :restaurants, :photo_file_size,    :integer
    add_column :restaurants, :photo_updated_at,   :datetime
  end
end
