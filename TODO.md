Security stuff:
do env variables on aws, facebook auth

Stuff

Homepage: 
DONE - Header with logo, login and register,
DONE - User register (Login, name, password),
DONE - List of Dinners (social dinners bought by people, sorted by date. Other types of sorting are welcome).

Dinner Page:
DONE Have a picture, name, date, description, address and google maps module associated,
DONE Faces of the people who have already purchased,
DONE - Call to action button for purchase,

Admin Homepage:
DONE - Dinner Creation
DONE - Admin Dinner List

References:
- www.tableandfriends.com
- www.grubwithus.com


  1. Ensure you have defined default url options in your environments files. Here 
     is an example of default_url_options appropriate for a development environment 
     in config/environments/development.rb:
       config.action_mailer.default_url_options = { :host => 'localhost:3000' }
     In production, :host should be set to the actual host of your application.
  3. Ensure you have flash messages in app/views/layouts/application.html.erb.
     For example:
       <p class="notice"><%= notice %></p>
       <p class="alert"><%= alert %></p>
  4. If you are deploying Rails 3.1 on Heroku, you may want to set:
       config.assets.initialize_on_precompile = false
     On config/application.rb forcing your application to not access the DB
     or load models when precompiling your assets.
